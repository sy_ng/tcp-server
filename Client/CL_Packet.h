#pragma once
#include "../Shared/BasePacket.h"

class RequestAuthPacket : public BaseRequest
{
public:
	RequestAuthPacket( const char *pUserName, const unsigned char *pPassword_SHA256 )
	{
		m_PacketType = CL_REQUEST_AUTH;
		lstrcpyA( m_UserName, pUserName );
		memcpy( m_Password_SHA256, pPassword_SHA256, 32 ); 

		m_hEvent = CreateEventA( NULL, TRUE, FALSE, NULL );
	}
	virtual					~RequestAuthPacket() {}

	virtual void			Serialize();
	virtual void			DeserializeResponse( unsigned char *pData, unsigned long nSize );
	virtual bool			HasReply() { return 1; }

	void					WaitResponse();

protected:
	char					m_UserName[ 32 ] = { 0 };
	unsigned char			m_Password_SHA256[ 32 ] = { 0 };
};

class RequestRegPacket : public BaseRequest
{
public:
	RequestRegPacket( const char *pUserName, const unsigned char *pPassword_SHA256 )
	{
		m_PacketType = CL_REQUEST_REG;
		lstrcpyA( m_UserName, pUserName );
		memcpy( m_Password_SHA256, pPassword_SHA256, 32 );

		m_hEvent = CreateEventA( NULL, TRUE, FALSE, NULL );
	}
	virtual					~RequestRegPacket() {}

	virtual void			Serialize();
	virtual void			DeserializeResponse( unsigned char *pData, unsigned long nSize );
	virtual bool			HasReply() { return 1; }

	void					WaitResponse();

protected:
	char					m_UserName[ 32 ] = { 0 };
	unsigned char			m_Password_SHA256[ 32 ] = { 0 };
};


class SendMsgPacket : public BaseRequest
{
public:
	SendMsgPacket( const char *pMessage )
	{
		m_PacketType = REQUEST_SEND_MSG;
		m_DataSize = lstrlenA( pMessage ) + 1;
		lstrcpyA( m_Message, pMessage );
	}
	virtual ~SendMsgPacket() {}

	virtual void Serialize();
	virtual void DeserializeResponse( unsigned char *pData, unsigned long nSize ) {};

protected:
	char m_Message[ 512 ] = { 0 };
};


/* ---- Handler Classes ---- */

class SendMsgHandler : public BaseRequestHandler
{
public:
	SendMsgHandler()
	{
		m_PacketType = REQUEST_SEND_MSG;
	}
	virtual							~SendMsgHandler() {}
	virtual unsigned long			GetType() { return m_PacketType; };
	virtual bool					DeserializePacket( BaseNetChannel *pChannel, buf_read &packet );
};