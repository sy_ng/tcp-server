#include "CL_Client.h"
#include "../Shared/sha256.h"
#include <conio.h>

#pragma warning(disable:4996)

CL_Client	g_client;
char		g_login[ 33 ] = { 0 };
char		g_password[ 33 ] = { 0 };
char		g_input[ 512 ] = { 0 };

void askCredits()
{
	printf( "Username: " );
	scanf( " %[^\n]s", g_login );

	printf( "Password: " );
	int i = 0;
	char c = 0;
	while ( i < sizeof( g_password ) )
	{
		c = _getch();
		if ( c == 13 )
		{
			printf( "\n" );
			break;
		}

		if ( c == '\b' && i != 0 )
		{
			g_password[ i-- ] = 0;
			printf( "\b \b" );
			continue;
		}
		else if ( c == '\b' && i == 0 )
			continue;

		printf( "*" );
		g_password[ i ] = c;
		++i;
	}
}

void messageLoop()
{
	while ( true )
	{
		printf( "[%s]: ", g_login );
		scanf( " %[^\n]s", g_input );

		if ( !lstrcmpA( g_input, "--exit" ) )
			return;

		g_client.SendMsg( g_input );
	}
}

int main()
{
	//char host[ 64 ] = { 0 };
	//printf( "Host: " );
	//scanf( " %[^\n]s", host );

	//char port[ 32 ] = { 0 };
	//printf( "Port: " );
	//scanf( " %[^\n]s", port );

	g_client.SetConnectAddress( "127.0.0.1", "27015" );
	if ( g_client.Connect() )
	{
		printf( "Connected\n" );
	}
	else
	{
		//client.Shutdown();
		printf( "Unable to connect\n" );
		return 0;
	}

	printf( "Enter command: " );
	char command[ 64 ] = { 0 };
	scanf( " %[^\n]s", command );

	char login[ 32 ] = { 0 };
	char password[ 32 ] = { 0 };

	if ( !lstrcmpA( command, "register" ) )
	{
		askCredits();
		g_client.SetUserName( g_login );
		if ( !g_client.Register( g_password ) )
		{
			return 0;
		}
		else
		{
			messageLoop();
		}
			
	}
	else if ( !lstrcmpA( command, "login" ) )
	{
		askCredits();
		g_client.SetUserName( g_login );
		if ( !g_client.Authorize( g_password ) )
		{
			return 0;
		}
		else
		{
			messageLoop();
		}
	}
	else if ( !lstrcmpA( command, "exit" ) )
	{
		//client.Shutdown();
		return 0;
	}

	return 0;
}