#pragma once
#include "../Shared/BaseNetChannel.h"
#include "../Shared/Vector.h"
#include "CL_Packet.h"

#define SERVER_ADDRESS "127.0.0.1"
#define SERVER_PORT "27015"

class CL_Client : BaseNetChannel
{
public:
	CL_Client() {}
	~CL_Client() {}

	virtual void	SetUserName( const char *pUsername );
	virtual void	SetConnectAddress( const char *pszServerAddress, const char *pszServerPort );
	virtual bool	Connect();

	virtual void	SendMsg( const char *pMessage  );
	virtual bool	Authorize( const char *pPassword );
	virtual bool	Register( const char *pPassword );

	virtual void	PrintMessage( const char *pUsername, const char *pMessage );

protected:
	SendMsgHandler	m_SendMsgHandler;

	char			m_pszServerAddress[ 32 ] = { 0 };
	char			m_pszServerPort[ 16 ] = { 0 };
	unsigned long	m_isAuthorised = 0;
	char			m_Username[ 32 ] = { 0 };
};