#include "CL_Client.h"
#include "../Shared/sha256.h"


void CL_Client::SetUserName( const char *pUsername )
{
	lstrcpyA( m_Username, pUsername );
}

void CL_Client::SetConnectAddress( const char *pszServerAddress, const char *pszServerPort )
{
	lstrcpyA( m_pszServerAddress, pszServerAddress );
	lstrcpyA( m_pszServerPort, pszServerPort );
}

bool CL_Client::Connect()
{
	RegisterHandler( &m_SendMsgHandler );
	// Initialization
	WSADATA wsaData;
	struct addrinfo hints;

	// Initialize Winsock
	printf( "CL_Client: WSAStartup: " );

	int iResult = WSAStartup( MAKEWORD( 2, 2 ), &wsaData );
	if ( iResult != 0 )
	{
		printf( "WSAStartup failed with error: %d\n", iResult );
		return 0;
	}
	printf( "OK\n" );

	memset( &hints, 0, sizeof( hints ) );
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	printf( "CL_Client: Resolving server at %s:%s: ", m_pszServerAddress, m_pszServerPort );

	iResult = getaddrinfo( m_pszServerAddress, m_pszServerPort, &hints, &m_pAddress );
	if ( iResult != 0 )
	{
		printf( "getaddrinfo failed with error: %d\n", iResult );
		WSACleanup();
		return 0;
	}
	printf( "OK\n" );

	printf( "CL_Client: Connecting to server: " );

	// Attempt to connect to an address until one succeeds
	struct addrinfo *ptr = NULL;
	for ( ptr = m_pAddress; ptr != NULL; ptr = ptr->ai_next )
	{
		// Create a SOCKET for connecting to server
		m_Socket = socket( ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol );
		if ( m_Socket == INVALID_SOCKET )
		{
			printf( "socket failed with error: %ld\n", WSAGetLastError() );
			WSACleanup();
			return 0;
		}

		// Connect to server.
		int nResult = connect( m_Socket, ptr->ai_addr, ( int ) ptr->ai_addrlen );
		if ( nResult == SOCKET_ERROR )
		{
			closesocket( m_Socket );
			m_Socket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	if ( m_Socket == INVALID_SOCKET )
	{
		printf( "unable to connect to server!\n" );
		WSACleanup();
		return 0;
	}
	printf( "OK\n" );

	printf( "CL_Client: Creating think thread: " );

	m_hThread = CreateThread( 0, 0, Net_RunRemoteThread, this, 0, NULL );
	if ( !m_hThread )
	{
		printf( "unable to create thread!\n" );
		return 0;
	}
	printf( "OK\n" );
	return 1;
}

void CL_Client::SendMsg( const char *pMessage )
{
	SendMsgPacket clientMessage( pMessage );
	SendRequest( clientMessage );
}
 
bool CL_Client::Authorize( const char *pPassword )
{
	unsigned char password_SHA256[ 32 ] = { 0 };
	SHA256_StringToBinaryHash( pPassword, password_SHA256 );

	RequestAuthPacket authPacket( ( const char * ) m_Username, password_SHA256 );
	SendRequest( authPacket );

	authPacket.WaitResponse();

	switch ( authPacket.GetResponse() )
	{
	case AUTH_RESP_USER_NOT_FOUND:
	{
		printf( "Username [%s] not found\n", m_Username );
		return 0;
	}
	case AUTH_RESP_SUCCESS:
	{
		printf( "Welcome back, [%s]!\n", m_Username );
		return 1;
	}
	case AUTH_RESP_PASS_MISMATCH:
	{
		printf( "[%s] Wrong password\n", m_Username );
		return 0;
	}
	default:
		return 0;
	}
		
}

bool CL_Client::Register( const char *pPassword )
{
	unsigned char password_SHA256[ 32 ] = { 0 };
	SHA256_StringToBinaryHash( pPassword, password_SHA256 );

	RequestRegPacket regPacket( ( const char * ) m_Username, password_SHA256 );
	SendRequest( regPacket );

	regPacket.WaitResponse();

	switch ( regPacket.GetResponse() )
	{
	case REG_RESP_USER_EXISTS:
	{
		printf( "User [%s] already exists. Try another one\n", m_Username );
		return 0;
	}
	case REG_RESP_SUCCESS:
	{
		printf( "User [%s] Successfully registered\n", m_Username );
		return 1;
	}

	default:
		return 0;
	}
}

void CL_Client::PrintMessage( const char* pUsername, const char *pMessage )
{
	printf( "[%s]: %s\n", pUsername, pMessage );
}