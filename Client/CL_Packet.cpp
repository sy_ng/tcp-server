#include "../Shared/utils_bitbuf.h"
#include "CL_Packet.h"
#include "CL_Client.h"


/* ---- Request Auth Packet ---- */
void RequestAuthPacket::Serialize()
{
	m_DataSize = lstrlenA( m_UserName ) + 1 + sizeof( m_Password_SHA256 );
	m_nPacketSize = sizeof( m_PacketType ) + sizeof( m_nSequenceNum ) + sizeof( m_DataSize ) + m_DataSize;

	m_Data.Append( m_PacketType );
	m_Data.Append( m_nSequenceNum );
	m_Data.Append( m_DataSize );
	m_Data.Append( ( const char *) m_UserName );
	m_Data.Append( m_Password_SHA256 );
};

void RequestAuthPacket::DeserializeResponse( unsigned char *pData, unsigned long nSize )
{
	m_ResponseResult = *( unsigned long * ) pData;
	SetEvent( m_hEvent );
}

void RequestAuthPacket::WaitResponse()
{
	WaitForSingleObject( m_hEvent, INFINITE );
}

/* ----- Request Reg Packet -----  */
void RequestRegPacket::Serialize()
{
	m_DataSize = lstrlenA( m_UserName ) + 1 + sizeof( m_Password_SHA256 );
	m_nPacketSize = sizeof( m_PacketType ) + sizeof( m_nSequenceNum ) + sizeof( m_DataSize ) + m_DataSize;

	m_Data.Append( m_PacketType );
	m_Data.Append( m_nSequenceNum );
	m_Data.Append( m_DataSize );
	m_Data.Append( ( const char * ) m_UserName );
	m_Data.Append( m_Password_SHA256 );
};

void RequestRegPacket::DeserializeResponse( unsigned char *pData, unsigned long nSize )
{
	m_ResponseResult = *( unsigned long * ) pData;
	SetEvent( m_hEvent );
}

void RequestRegPacket::WaitResponse()
{
	WaitForSingleObject( m_hEvent, INFINITE );
}


/* ---- Send Message Packet ---- */
void SendMsgPacket::Serialize()
{
	// add sender username to packet data
	m_nPacketSize = sizeof( m_PacketType ) + sizeof( m_nSequenceNum ) + sizeof( m_DataSize ) + m_DataSize;

	m_Data.Append( m_PacketType );
	m_Data.Append( m_nSequenceNum );
	m_Data.Append( m_DataSize );
	m_Data.Append( ( const char * ) m_Message );
}


/* ---- Send Message Handler ---- */
bool SendMsgHandler::DeserializePacket( BaseNetChannel *pChannel, buf_read &packet )
{
	unsigned long	nSequenceNum;
	unsigned long	nDataSize;

	packet.Read( nSequenceNum );
	packet.Read( nDataSize );

	if ( nDataSize > packet.GetNumBytesLeft() )
	{
		printf( "NetChannel: Failed to Deserlize, SEND_MSG packet is not complete\n" );
		return 0;
	}

	char username[ 32 ] = { 0 };
	char message[ 512 ] = { 0 };
	packet.Read( ( char * ) username, sizeof( username ));
	packet.Read( ( char * ) message, sizeof( message ));

	CL_Client &pClient = *( CL_Client * ) pChannel;
	pClient.PrintMessage( username, message );

	return 1;
}