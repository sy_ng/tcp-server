#include "SV_Server.h"
#include "SV_Packet.h"
#include "SV_Client.h"

SV_Server::SV_Server( const unsigned long nServerMaxClients ): 
	m_SendMsgHandler( this ), 
	m_RequestRegHandler( this ), 
	m_RequestAuthHandler( this )
{
	m_nServerMaxClients = nServerMaxClients;
	m_UserDataStorage.Load();
}

SV_Server::~SV_Server(){}

void SV_Server::WaitForConnections()
{
	WSADATA wsaData;
	int iResult;

	//struct addrinfo *result = NULL;
	struct addrinfo hints;

	// Initialize Winsock
	printf( "SV_Server: WSAStartup: " );
	iResult = WSAStartup( MAKEWORD( 2, 2 ), &wsaData );
	if ( iResult != 0 )
	{
		printf( "failed with error: %d\n", iResult );
		return;
	}
	printf( "OK\n" );

	ZeroMemory( &hints, sizeof( hints ) );
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	printf( "SV_Server: Resolving server address: " );
	iResult = getaddrinfo( NULL, SERVER_PORT, &hints, &m_pAddress  );
	if ( iResult != 0 )
	{
		printf( "getaddrinfo failed with error: %d\n", iResult );
		WSACleanup();
		return;
	}
	printf( "OK\n" );

	// Create a SOCKET for connecting to server
	printf( "SV_Server: Creating server socket: " );
	m_Socket = socket( m_pAddress->ai_family, m_pAddress->ai_socktype, m_pAddress->ai_protocol );
	if ( m_Socket == INVALID_SOCKET )
	{
		printf( "socket failed with error: %ld\n", WSAGetLastError() );
		freeaddrinfo( m_pAddress );
		WSACleanup();
		return;
	}
	printf( "OK\n" );

	// Setup the TCP listening socket
	printf( "SV_Server: Binding server socket: " );
	iResult = bind( m_Socket, m_pAddress->ai_addr, ( int ) m_pAddress->ai_addrlen );
	if ( iResult == SOCKET_ERROR )
	{
		printf( "failed with error: %d\n", WSAGetLastError() );
		freeaddrinfo( m_pAddress );
		closesocket( m_Socket );
		WSACleanup();
		return;
	}
	printf( "OK\n" );

	printf( "SV_Server: Listen socket: " );
	int nResult = listen( m_Socket, SOMAXCONN );
	if ( nResult == SOCKET_ERROR )
	{
		printf( "SV_Server failed: Listen failed with error: %d\n", WSAGetLastError() );
		return;
	}
	printf( "OK\n" );

	freeaddrinfo( m_pAddress );

	// Main connection handling loop
	printf( "SV_Server: Waiting for connections\n" );
	while( true )
	{
		if ( m_ServerClients.Length() >= SERVER_MAX_CLIENTS )
		{
			// Server is full
			Sleep( 100 );
			continue;
		}

	   	SOCKET hClientSocket = accept( m_Socket, NULL, NULL );
		if ( hClientSocket == INVALID_SOCKET )
		{
			printf( "SV_Server: Accept failed with error: %d\n", WSAGetLastError() );
			return;
		}

		SV_Client *newClient = CreateNewClient();

		if ( !newClient->Init( hClientSocket ) )
		{
			printf( "SV_Server: cannot initialize client\n" );
			return;
		}
		printf( "SV_Server: client connected\n" );
	}
}

SV_Client* SV_Server::CreateNewClient()
{
	SV_Client *newClient = new SV_Client(this);
	m_ServerClients.PushBack( newClient );

	newClient->RegisterHandler( &m_RequestAuthHandler );
	newClient->RegisterHandler( &m_RequestRegHandler );
	newClient->RegisterHandler( &m_SendMsgHandler );

	return newClient;
}

int SV_Server::AuthorizeClient( const char *pUsername, const unsigned char *pPassword_SHA256 )
{
	return m_UserDataStorage.FindUser( pUsername, pPassword_SHA256 );
}

void SV_Server::RegisterClient( const char *pUsername, const unsigned char *pPassword_SHA256 )
{
	m_UserDataStorage.AddUser( pUsername, pPassword_SHA256 );
}

void SV_Server::SendMessageToAllClients( SV_Client *author, const char *pMessage )
{
	m_ServerClients.Lock();
	for ( unsigned long i = 0; i < m_ServerClients.Length(); ++i )
	{
		if ( m_ServerClients[ i ] == author )
			continue;

		m_ServerClients[ i ]->SendMsg( author, pMessage );
	}
	m_ServerClients.Unlock();
}