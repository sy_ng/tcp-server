#include "../Shared/utils_bitbuf.h"
#include "SV_Packet.h"
#include "SV_Client.h"
#include "SV_Server.h"


void SendMsgPacket::Serialize()
{
	// add sender username to packet data
	m_nPacketSize = sizeof( m_PacketType ) + sizeof( m_nSequenceNum ) + sizeof( m_DataSize ) + m_DataSize;

	m_Data.Append( m_PacketType );
	m_Data.Append( m_nSequenceNum );
	m_Data.Append( m_DataSize );
	m_Data.Append( ( const char * ) m_Username );
	m_Data.Append( ( const char * ) m_Message );
}

bool RequestAuthHandler::DeserializePacket( BaseNetChannel *pChannel, buf_read &packet )
{
	//printf( "CL_REQUEST_AUTH packet recieved\n" );

	unsigned long	nSequenceNum;
	unsigned long	nDataSize;

	packet.Read( nSequenceNum );
	packet.Read( nDataSize );

	if ( packet.GetNumBytesLeft() < nDataSize )
	{
		// should return PACKET_NOT_SUFFICIENT
		printf( "RequestAuthHandler: packet not sufficient\n" );
		return 0;
	}

	char			username[ 32 ];
	unsigned char	password_SHA256[ 32 ];

	packet.Read( username, 32 );
	packet.Read( password_SHA256 );

	SV_Client &client = *( SV_Client * ) pChannel;

	unsigned long respBody;
	int authRet = m_pServer->AuthorizeClient( username, password_SHA256 );

	// 0 - user not found
	// 1 - user found, passwords match
	// 2 - user found, passwords do no match
	switch ( authRet )
	{
	case 0:
	{
		printf( "[%s] login attempt, user not found\n", username );
		respBody = AUTH_RESP_USER_NOT_FOUND;
		break;
	}
	case 1:
	{
		printf( "[%s] authorized\n", username );
		client.Authorize( username );
		respBody = AUTH_RESP_SUCCESS;
		break;
	}
	case 2:
	{
		printf( "[%s] login attempt, wrong password\n", username );
		respBody = AUTH_RESP_PASS_MISMATCH;
		break;
	}

	default:
		break;
	}

	Response response( &respBody, sizeof(respBody) );
	response.SetSequenceNum( nSequenceNum );
	pChannel->SendResponse( response );

	return 1;
}

bool RequestRegHandler::DeserializePacket( BaseNetChannel *pChannel, buf_read &packet )
{
	//printf( "CL_REQUEST_REG packet recieved\n" );

	unsigned long	nSequenceNum;
	unsigned long	nDataSize;

	packet.Read( nSequenceNum );
	packet.Read( nDataSize );

	if ( packet.GetNumBytesLeft() < nDataSize )
	{
		// should return PACKET_NOT_SUFFICIENT
		printf( "RequestAuthHandler: packet not sufficient\n" );
		return 0;
	}

	char			username[ 32 ];
	unsigned char	password_SHA256[ 32 ];

	packet.Read( username, 32 );
	packet.Read( password_SHA256 );

	unsigned long respBody;
	int authRet = m_pServer->AuthorizeClient( username, password_SHA256 );

	SV_Client &client = *( SV_Client * ) pChannel;

	if ( authRet == 0 )
	{
		printf( "[%s] new user\n", username );
		m_pServer->RegisterClient( username, password_SHA256 );
		client.Authorize( username );
		respBody = REG_RESP_SUCCESS;
	}
	else if ( authRet == 1 || authRet == 2 )
	{
		printf( "[%s] reg attempt, user exists\n", username );
		respBody = REG_RESP_USER_EXISTS;
	}

	Response response( &respBody, sizeof( respBody ) );
	response.SetSequenceNum( nSequenceNum );
	pChannel->SendResponse( response );

	return 1;
}

bool SendMsgHandler::DeserializePacket( BaseNetChannel *pChannel, buf_read &packet )
{
	//printf( "REQUEST_SEND_MSG packet recieved\n" );

	SV_Client &client = *( SV_Client * ) pChannel;
	if ( !client.IsAuthorized() )
	{
		printf( "Attempt sending message from unauthorized user detected\n" );
		return 0;
	}

	unsigned long	nSequenceNum;
	unsigned long	nDataSize;

	packet.Read( nSequenceNum );
	packet.Read( nDataSize );

	if ( packet.GetNumBytesLeft() < nDataSize )
	{
		// should return PACKET_NOT_SUFFICIENT
		printf( "RequestAuthHandler: packet not sufficient\n" );
		return 0;
	}

	char pMessage[ 512 ] = { 0 };
	packet.Read( ( void * ) pMessage, nDataSize );

	client.PrintMessage( pMessage );
	m_pServer->SendMessageToAllClients( &client,  pMessage );

	return 1;
}