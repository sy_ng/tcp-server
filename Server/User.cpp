#include <stdio.h>
#include "User.h"
#include "../Shared/utils_bitbuf.h"
#include "../Shared/sha256.h"

User::User( buf_read &User )
{
	User.Read( m_nUserId );
	User.Read( m_szName, sizeof( m_szName ) );
	User.Read( m_pwd_sha256 );
}

User::User( const unsigned long nUserId, const char *pszName, const unsigned char *pPassword_SHA256 )
{
	m_nUserId = nUserId;
	lstrcpyA( m_szName, pszName );
	memcpy( m_pwd_sha256, pPassword_SHA256, 32 );
}

User::~User()
{
}

void User::Serialize( Vector<unsigned char> &storageData )
{
	storageData.Lock();

	storageData.Append( m_nUserId );
	storageData.Append( ( const char * ) m_szName );
	storageData.Append( m_pwd_sha256 );

	storageData.Unlock();
}

void User::SetSavedState()
{
	m_isSaved = true;
}

bool User::GetSavedState()
{
	return m_isSaved;
}

//Vector<unsigned char> *User::GetSerialized()
//{
//	m_Body.Append( m_nUserId );
//	m_Body.Append( ( const char * ) m_szName );
//	m_Body.Append( m_pwd_sha256 );
//
//	return &m_Body;
//}

void User::Print()
{
	printf( "UserId: %i\n", m_nUserId );
	printf( "Username is: %s\n", m_szName );

	char stringSHA[ 65 ] = { 0 };
	SHA256_BinaryToString( m_pwd_sha256, stringSHA );

	printf( "Password SHA is: %s\n", stringSHA );
}

//
//void User::SetUserData( const unsigned long nUserId, const char *pszName, const unsigned char *pPassword_SHA256 )
//{
//	m_nUserId = nUserId;
//	lstrcpyA( m_szName, pszName );
//	memcpy( &m_pwd_sha256, pPassword_SHA256, 32 );
//}

