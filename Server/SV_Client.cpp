#include "SV_Client.h"
#include "SV_Packet.h"
#include "SV_Server.h"
#include "../Shared/BaseNetChannel.h"

bool SV_Client::Init( SOCKET hClientSocket )
{
	m_Socket = hClientSocket;
	m_hThread = CreateThread( 0, 0, Net_RunRemoteThread, this, 0, NULL );
	if ( !m_hThread )
	{
		printf( "NetChannel: Error while creating thread\n" );
		return 0;
	}

	return 1;
}

bool SV_Client::Shutdown()
{
	int nResult;
	nResult = shutdown( m_Socket, SD_BOTH );
	if ( nResult == SOCKET_ERROR )
	{
		printf( "NetChannel: shutdown failed with error: %d\n", WSAGetLastError() );
		closesocket( m_Socket );
		WSACleanup();
		return 0;
	}
	nResult = closesocket( m_Socket );
	if ( nResult )
	{
		printf( "NetChannel: closesocket failed with error:%d\n", WSAGetLastError() );
		return 0;
	}
	return 1;
}

void SV_Client::PrintMessage( const char* pMessage )
{
	printf("[%s]: %s\n", m_Username, pMessage);
}

void SV_Client::Authorize( const char *pUsername )
{
	m_isAuthorized = true;
	lstrcpyA( m_Username, pUsername );
	printf( "[%s] authorized\n", m_Username );
}

void SV_Client::SendMsg( SV_Client *author, const char *pMessage )
{
	SendMsgPacket clientMessage( author->m_Username, pMessage );
	SendRequest( clientMessage );
}
