#pragma once
#include "../Shared/Vector.h"
#include "../Shared/BasePacket.h"

// Request Auth responses
#define AUTH_RESP_USER_NOT_FOUND	0xFFFF0000
#define AUTH_RESP_SUCCESS			0xFFFF0001
#define AUTH_RESP_PASS_MISMATCH		0xFFFF0002

// Request Reg responses
#define REG_RESP_USER_EXISTS		0xFFFF0000
#define REG_RESP_SUCCESS			0xFFFF0001

class SV_Server;


/* ---- Packet Classes ---- */

class SendMsgPacket : public BaseRequest
{
public:
	SendMsgPacket( const char *pUsername, const char *pMessage )
	{
		m_PacketType = REQUEST_SEND_MSG;
		m_DataSize = lstrlenA( pUsername ) + 1 + lstrlenA( pMessage ) + 1;
		lstrcpyA( m_Username, pUsername );
		lstrcpyA( m_Message, pMessage );
	}
	virtual ~SendMsgPacket() {}

	virtual void Serialize();
	virtual void DeserializeResponse( unsigned char *pData, unsigned long nSize ) {};

protected:
	char m_Message[ 512 ] = { 0 };
	char m_Username[ 32 ] = { 0 };
};


/* ---- Handler Classes ---- */

class RequestAuthHandler : public BaseRequestHandler
{
public:
	RequestAuthHandler( SV_Server *pServer )
	{
		m_pServer = pServer;
		m_PacketType = CL_REQUEST_AUTH;
	}
	virtual							~RequestAuthHandler() {}
	virtual unsigned long			GetType() { return m_PacketType; };

	virtual void					Serialize() {}
	virtual bool					DeserializePacket( BaseNetChannel *pChannel, buf_read &packet );

protected:
	SV_Server *m_pServer = NULL;
};

class RequestRegHandler : public BaseRequestHandler
{
public:
	RequestRegHandler( SV_Server *pServer )
	{
		m_pServer = pServer;
		m_PacketType = CL_REQUEST_REG;
	}
	virtual							~RequestRegHandler() {}
	virtual unsigned long			GetType() { return m_PacketType; };

	virtual void					Serialize() {}
	virtual bool					DeserializePacket( BaseNetChannel *pChannel, buf_read &packet );

protected:
	SV_Server *m_pServer = NULL;
};

class SendMsgHandler : public BaseRequestHandler
{
public:
	SendMsgHandler( SV_Server *pServer )
	{
		m_pServer = pServer;
		m_PacketType = REQUEST_SEND_MSG;
	}
	virtual							~SendMsgHandler() {}
	//virtual unsigned long			GetType() { return m_PacketType; };

	virtual void					Serialize() {}
	virtual bool					DeserializePacket( BaseNetChannel *pChannel, buf_read &packet );

protected:
	SV_Server *m_pServer = NULL;
};