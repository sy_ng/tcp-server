#include "storage.h"
#include "../Shared/utils_file.h"
#include "../Shared/utils_bitbuf.h"
#include "User.h"

Storage::Storage( const char *pStorageFilePath )
{
	m_pStorageFilePath = new char[ lstrlenA( pStorageFilePath ) + 1 ];
	lstrcpyA( m_pStorageFilePath, pStorageFilePath );
}

Storage::~Storage() {}


void Storage::Load()
{
	unsigned char *pStorageFileData = NULL;
	unsigned long storageFileSize = 0;

	LoadFile( m_pStorageFilePath, ( void ** ) &pStorageFileData, &storageFileSize );

	buf_read storageData;
	storageData.Init( pStorageFileData, storageFileSize );

	while ( storageData.GetNumBytesLeft() )
	{
		User *user = new User( storageData );
		m_UsersList.PushBack( user );
	}

	Serialize();
}

void Storage::Save()
{
	Serialize();
	SaveFile( m_pStorageFilePath, m_StorageData.Base(), m_StorageData.Length() );
}

void Storage::Serialize()
{
	m_UsersList.Lock();
	for ( unsigned int i = 0; i < m_UsersList.Length(); ++i )
	{
		if ( ! ( m_UsersList[i]->GetSavedState() ) )
		{
			m_UsersList[ i ]->Serialize( m_StorageData );
			m_UsersList[ i ]->SetSavedState();
		}
	}
	m_UsersList.Unlock();
}

void Storage::AddUser( const char *pUsername, const unsigned char *pPassword_SHA256 )
{
	User *newUser = new User( m_UsersList.Length(), pUsername, pPassword_SHA256 );
	m_UsersList.PushBack( newUser );
	Save();
}

void Storage::PrintUsers()
{
	for ( unsigned int i = 0; i < m_UsersList.Length(); i++ )
	{
		m_UsersList[ i ]->Print();
	}
}

int Storage::FindUser( const char *pszUsername, const unsigned char *pPassword_SHA256 )
{
	// ret values:
	// 0 - user not found
	// 1 - user found, passwords match
	// 2 - user found, passwords do no match

	m_UsersList.Lock();

	for ( unsigned int i = 0; i < m_UsersList.Length(); i++ )
	{
		if ( ! lstrcmpA( m_UsersList[ i ]->GetName(), pszUsername ) )
		{
			if ( ! memcmp( m_UsersList[ i ]->GetPassword_SHA256(), pPassword_SHA256, 32 ) )
			{
				m_UsersList.Unlock();
				return 1;
			}
			else
			{
				m_UsersList.Unlock();
				return 2;
			}
		}
	}

	m_UsersList.Unlock();
	return 0;
}