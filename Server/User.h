#pragma once
#include "../Shared/Vector.h"

class buf_read;

class User
{
public:
	User( const unsigned long nUserId, const char *pszName, const unsigned char *pPassword_SHA256 );
	User( buf_read &userData );
	~User();

	bool operator==( const User &rUser )
	{
		return ( !memcmp( m_szName, rUser.m_szName, lstrlenA( m_szName ) ) && !memcmp( m_pwd_sha256, rUser.m_pwd_sha256, 32 ) );
	}

	void					Print();
	void					Serialize( Vector<unsigned char> &storageData );

	void					SetSavedState();
	bool					GetSavedState();

	char					*GetName() { return m_szName; }
	unsigned char			*GetPassword_SHA256() { return m_pwd_sha256; }

private:
	bool					m_isSaved = 0; // if saved to raw data, no need to copy while serialization
	unsigned long			m_nUserId = 0;
	char					m_szName[ 32 ] = { 0 };
	unsigned char			m_pwd_sha256[ 32 ] = { 0 };

	Vector<unsigned char>	m_Body;
};

