#pragma once
#include "../Shared/BaseNetChannel.h"

class SV_Server;

class SV_Client : public BaseNetChannel
{
public:
	SV_Client( SV_Server *pServer ) : m_pServer( pServer ) {}
	virtual					~SV_Client(){}

	virtual bool			Init( SOCKET clientSocket );
	virtual bool			Shutdown();

	virtual void			PrintMessage( const char *pMessage );
	virtual void			SendMsg( SV_Client *author, const char *pMessage );

	//virtual bool			Authorize( const char *pUserName, unsigned char *pPassword_SHA256 );
	void					Authorize( const char *pUsername );
	virtual bool			IsAuthorized() { return m_isAuthorized; }

private:
	SV_Server				*m_pServer = NULL;
	bool					m_isAuthorized = 0;
	char					m_Username[ 32 ] = { 0 };
	//unsigned char			m_Password_SHA256[ 32 ] = { 0 };
};
