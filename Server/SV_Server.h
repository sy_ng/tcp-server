#pragma once
#include <Windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>

#include "../Shared/Vector.h"
#include "../Shared/BaseNetChannel.h"
#include "SV_Packet.h"
#include "Storage.h"

class SV_Client;

#define SERVER_MAX_CLIENTS 64
#define SERVER_PORT "27015"

class SV_Server : public BaseNetChannel
{
public:
	SV_Server( const unsigned long nServerMaxClients = SERVER_MAX_CLIENTS );
	~SV_Server();

	virtual void			WaitForConnections();
	virtual SV_Client		*CreateNewClient();

	virtual	void			RegisterClient( const char *pUsername, const unsigned char *pPassword_SHA256 );
	virtual int				AuthorizeClient( const char *pUsername, const unsigned char *pPassword_SHA256 );

	virtual void			SendMessageToAllClients( SV_Client *author, const char *pMessage );

protected:
	unsigned long			m_nServerMaxClients = 0;
	Vector<SV_Client *>		m_ServerClients;

	RequestAuthHandler		m_RequestAuthHandler;
	RequestRegHandler		m_RequestRegHandler;
	SendMsgHandler			m_SendMsgHandler;

	Storage					m_UserDataStorage;
};



