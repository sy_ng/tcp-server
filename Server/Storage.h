#pragma once
#include <Windows.h>
#include "../Shared/Vector.h"
#include "../Shared/sha256.h"

#define STORAGE_FILE_PATH "userdata.st"

class User;

class Storage
{
public:
	Storage( const char *pStorageFilePath = STORAGE_FILE_PATH );
	~Storage();

	//bool SaveStorageFile();
	//bool AddUser( UserData *pData );
	//bool RemoveUser( UserData *pData );
	//bool GetUserById( unsigned long nUserId );

	void					Load();
	void					Save();

	void					Serialize();
	//void					AddUser( User *user );
	void					AddUser( const char *pUsername, const unsigned char *pPassword_SHA256 );
	int						FindUser( const char *pszUsername, const unsigned char *pPassword_SHA256 );
	void					PrintUsers();

private:
	char					*m_pStorageFilePath = NULL;
	Vector<User *>			m_UsersList;
	Vector<unsigned char>	m_StorageData;
};

