#pragma once
#include <Windows.h>

#define UNIQUE_CONCAT( a, b )	a##b
#define UNIQUE_CONCAT2( a, b )	UNIQUE_CONCAT( a, b )
#define TOSTR2( any )			#any
#define TOSTR( any )			TOSTR2( any )

class CriticalSection
{
public:
	CriticalSection()
	{
		m_pSection = new CRITICAL_SECTION;
		InitializeCriticalSection( m_pSection );
	}
	~CriticalSection()
	{
		DeleteCriticalSection( m_pSection );
		delete m_pSection;
	}

	void Lock()
	{
		EnterCriticalSection( m_pSection );
	}

	void Unlock()
	{
		LeaveCriticalSection( m_pSection );
	}

private:
	CRITICAL_SECTION *m_pSection;
};

class CriticalSectionLocker
{
public:
	CriticalSectionLocker( CriticalSection *pLock )
	{
		m_pLock = pLock;
		m_pLock->Lock();
	}

	~CriticalSectionLocker()
	{
		m_pLock->Unlock();
	}

protected:
	CriticalSection *m_pLock;
};

#define SECTION_AUTOLOCK( section ) \
	CriticalSectionLocker UNIQUE_CONCAT2( autoLocker, __LINE__ )( &section )

#define SECTION_AUTOLOCK_STATIC() \
	static CriticalSection section; \
	CriticalSectionLocker UNIQUE_CONCAT2( autoLocker, __LINE__ )( &section )
