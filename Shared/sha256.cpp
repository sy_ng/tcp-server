#include "windows.h"
#include "sha256.h"

#define NC_MARKER \
	__asm { push eax } \
	__asm { push ecx } \
	__asm { push ebx } \
	__asm { push 0 } \
	__asm { add eax, ecx } \
	__asm { nop } \
	__asm { xor ecx, ebx } \
	__asm { xor ecx, ecx } \
	__asm { nop } \
	__asm { inc ecx } \
	__asm { nop } \
	__asm { pop eax } \
	__asm { pop ebx } \
	__asm { dec ecx } \
	__asm { pop ecx } \
	__asm { pop eax }

#define SHA224_256_BLOCK_SIZE 64

#define SHA2_SHFR(x, n)    (x >> n)
#define SHA2_ROTR(x, n)   ((x >> n) | (x << ((sizeof(x) << 3) - n)))
#define SHA2_ROTL(x, n)   ((x << n) | (x >> ((sizeof(x) << 3) - n)))
#define SHA2_CH(x, y, z)  ((x & y) ^ (~x & z))
#define SHA2_MAJ(x, y, z) ((x & y) ^ (x & z) ^ (y & z))
#define SHA256_F1(x) (SHA2_ROTR(x,  2) ^ SHA2_ROTR(x, 13) ^ SHA2_ROTR(x, 22))
#define SHA256_F2(x) (SHA2_ROTR(x,  6) ^ SHA2_ROTR(x, 11) ^ SHA2_ROTR(x, 25))
#define SHA256_F3(x) (SHA2_ROTR(x,  7) ^ SHA2_ROTR(x, 18) ^ SHA2_SHFR(x,  3))
#define SHA256_F4(x) (SHA2_ROTR(x, 17) ^ SHA2_ROTR(x, 19) ^ SHA2_SHFR(x, 10))

#define SHA2_UNPACK32(x, str)                     \
{                                                 \
    *((str) + 3) = (unsigned char) ((x)      );   \
    *((str) + 2) = (unsigned char) ((x) >>  8);   \
    *((str) + 1) = (unsigned char) ((x) >> 16);   \
    *((str) + 0) = (unsigned char) ((x) >> 24);   \
}

#define SHA2_PACK32(str, x)                       \
{                                                 \
    *(x) =   ((unsigned int) *((str) + 3)      )  \
           | ((unsigned int) *((str) + 2) <<  8)  \
           | ((unsigned int) *((str) + 1) << 16)  \
           | ((unsigned int) *((str) + 0) << 24); \
}

struct SHA256_CTX
{
	unsigned char	m_block[ 2 * SHA224_256_BLOCK_SIZE ];
	unsigned int	m_h[ 8 ];
	unsigned int	m_tot_len;
	unsigned int	m_len;
};

void SHA256_Init( SHA256_CTX *ctx )
{
	NC_MARKER;

	ctx->m_h[ 0 ] = 0x6a09e667;
	ctx->m_h[ 1 ] = 0xbb67ae85;
	ctx->m_h[ 2 ] = 0x3c6ef372;
	ctx->m_h[ 3 ] = 0xa54ff53a;
	ctx->m_h[ 4 ] = 0x510e527f;
	ctx->m_h[ 5 ] = 0x9b05688c;
	ctx->m_h[ 6 ] = 0x1f83d9ab;
	ctx->m_h[ 7 ] = 0x5be0cd19;
	ctx->m_len = 0;
	ctx->m_tot_len = 0;

	NC_MARKER;
}

void SHA256_Transform( SHA256_CTX *ctx, const unsigned char *message, unsigned int block_nb )
{
	NC_MARKER;

	const unsigned int sha256_k[ 64 ] = {
		0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
		0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
		0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
		0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
		0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
		0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
		0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
		0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
		0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
		0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
		0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
		0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
		0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
		0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
		0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
		0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
	};

	int i, j;
	unsigned int w[ 64 ];
	unsigned int wv[ 8 ];
	unsigned int t1, t2;
	const unsigned char *sub_block;

	for ( i = 0; i < (int) block_nb; i++ )
	{
		NC_MARKER;

		sub_block = message + ( i << 6 );

		for ( j = 0; j < 16; j++ )
			SHA2_PACK32( &sub_block[ j << 2 ], &w[ j ] );

		for ( j = 16; j < 64; j++ )
			w[ j ] = SHA256_F4( w[ j - 2 ] ) + w[ j - 7 ] + SHA256_F3( w[ j - 15 ] ) + w[ j - 16 ];

		for ( j = 0; j < 8; j++ )
			wv[ j ] = ctx->m_h[ j ];

		for ( j = 0; j < 64; j++ )
		{
			t1 = wv[ 7 ] + SHA256_F2( wv[ 4 ] ) + SHA2_CH( wv[ 4 ], wv[ 5 ], wv[ 6 ] ) + sha256_k[ j ] + w[ j ];
			t2 = SHA256_F1( wv[ 0 ] ) + SHA2_MAJ( wv[ 0 ], wv[ 1 ], wv[ 2 ] );
			wv[ 7 ] = wv[ 6 ];
			wv[ 6 ] = wv[ 5 ];
			wv[ 5 ] = wv[ 4 ];
			wv[ 4 ] = wv[ 3 ] + t1;
			wv[ 3 ] = wv[ 2 ];
			wv[ 2 ] = wv[ 1 ];
			wv[ 1 ] = wv[ 0 ];
			wv[ 0 ] = t1 + t2;
		}

		for ( j = 0; j < 8; j++ )
			ctx->m_h[ j ] += wv[ j ];

		NC_MARKER;
	}

	NC_MARKER;
}

void SHA256_Update( SHA256_CTX *ctx, const unsigned char *message, unsigned int len )
{
	NC_MARKER;

	unsigned int block_nb;
	unsigned int new_len, rem_len, tmp_len;
	const unsigned char *shifted_message;

	tmp_len = SHA224_256_BLOCK_SIZE - ctx->m_len;
	rem_len = len < tmp_len ? len : tmp_len;

	NC_MARKER;

	memcpy( &ctx->m_block[ ctx->m_len ], message, rem_len );

	if ( ctx->m_len + len < SHA224_256_BLOCK_SIZE )
	{
		ctx->m_len += len;
		return;
	}

	NC_MARKER;

	new_len = len - rem_len;
	block_nb = new_len / SHA224_256_BLOCK_SIZE;
	shifted_message = message + rem_len;

	NC_MARKER;

	SHA256_Transform( ctx, ctx->m_block, 1 );
	SHA256_Transform( ctx, shifted_message, block_nb );

	NC_MARKER;

	rem_len = new_len % SHA224_256_BLOCK_SIZE;
	memcpy( ctx->m_block, &shifted_message[ block_nb << 6 ], rem_len );

	NC_MARKER;

	ctx->m_len = rem_len;
	ctx->m_tot_len += ( block_nb + 1 ) << 6;

	NC_MARKER;
}

void SHA256_Final( SHA256_CTX *ctx, unsigned char *digest )
{
	NC_MARKER;

	unsigned int block_nb;
	unsigned int pm_len;
	unsigned int len_b;
	int i;

	NC_MARKER;

	block_nb = ( 1 + ( ( SHA224_256_BLOCK_SIZE - 9 ) < ( ctx->m_len % SHA224_256_BLOCK_SIZE ) ) );
	len_b = ( ctx->m_tot_len + ctx->m_len ) << 3;
	pm_len = block_nb << 6;
	RtlSecureZeroMemory( ctx->m_block + ctx->m_len, pm_len - ctx->m_len );
	ctx->m_block[ ctx->m_len ] = 0x80;

	NC_MARKER;

	SHA2_UNPACK32( len_b, ctx->m_block + pm_len - 4 );
	SHA256_Transform( ctx, ctx->m_block, block_nb );

	NC_MARKER;

	for ( i = 0; i < 8; i++ )
		SHA2_UNPACK32( ctx->m_h[ i ], &digest[ i << 2 ] );

	NC_MARKER;
}

void SHA256_DataToBinaryHash( const unsigned char *pInput, unsigned long nInputLength, unsigned char pOutput[ DIGEST_SIZE ] )
{
	NC_MARKER;

	SHA256_CTX ctx;
	SHA256_Init( &ctx );
	SHA256_Update( &ctx, pInput, nInputLength );
	SHA256_Final( &ctx, pOutput );

	NC_MARKER;
}

void SHA256_StringToBinaryHash( const char *pszInput, unsigned char pOutput[ DIGEST_SIZE ] )
{
	NC_MARKER;

	SHA256_DataToBinaryHash( (unsigned char *) pszInput, lstrlenA( pszInput ), pOutput );

	NC_MARKER;
}

void SHA256_StringToStringHash( const char *pszInput, char pOutput[ 2 * DIGEST_SIZE + 1 ] )
{
	NC_MARKER;

	unsigned char digest[ DIGEST_SIZE ];

	SHA256_StringToBinaryHash( pszInput, digest );
	SHA256_BinaryToString( digest, pOutput );

	NC_MARKER;
}

void SHA256_BinaryToString( unsigned char pInput[ DIGEST_SIZE ], char pOutput[ 2 * DIGEST_SIZE + 1 ] )
{
	NC_MARKER;

	for ( int i = 0; i < DIGEST_SIZE; ++i )
		wsprintfA( pOutput + i * 2, "%02x", pInput[ i ] );

	pOutput[ 2 * DIGEST_SIZE ] = 0;

	NC_MARKER;
}

void SHA256_StringToBinary( char pInput[ 2 * DIGEST_SIZE + 1 ], unsigned char pOutput[ DIGEST_SIZE ] )
{
	NC_MARKER;

	for ( int i = 0; i < DIGEST_SIZE; ++i )
	{
		unsigned char d = pInput[ i * 2 ];
		unsigned char n = pInput[ i * 2 + 1 ];

		if ( d >= '0' && d <= '9' )
			d -= '0';
		else if ( d >= 'a' && d <= 'f' )
			d -= 'a' - 10;

		if ( n >= '0' && n <= '9' )
			n -= '0';
		else if ( n >= 'a' && n <= 'f' )
			n -= 'a' - 10;

		pOutput[ i ] = d * 16 + n;
	}

	NC_MARKER;
}
