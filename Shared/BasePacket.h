#pragma once
#include "Vector.h"

// Client only requests
#define CL_REQUEST_AUTH				0xFFFFFFFF
#define CL_REQUEST_REG				0xFFFFFFFD

// All requests
#define REQUEST_SEND_MSG			0xFFFFFFFE

// Responses
#define RESPONSE					0xEEEEEEEE

#define AUTH_RESP_USER_NOT_FOUND	0xFFFF0000
#define AUTH_RESP_SUCCESS			0xFFFF0001
#define AUTH_RESP_PASS_MISMATCH		0xFFFF0002

#define REG_RESP_USER_EXISTS		0xFFFF0000
#define REG_RESP_SUCCESS			0xFFFF0001


class BaseNetChannel;
class buf_read;

/* ---- Base Classes ---- */
class BasePacket
{
public:
	virtual							~BasePacket() {}

	//virtual void					Serialize() = 0;
	virtual Vector<unsigned char>	&Data() { return m_Data; }
	virtual unsigned long			Size() { return m_nPacketSize; }
	virtual unsigned long			SequenceNum() { return m_nSequenceNum; }
	virtual void					SetSequenceNum( unsigned long nSequenceNum ) { m_nSequenceNum = nSequenceNum; }
	//virtual bool					HasReply() { return 0; }

protected:
	unsigned long					m_PacketType = 0;
	unsigned long					m_nSequenceNum = 0;
	unsigned long					m_nPacketSize = 0;
	unsigned long					m_DataSize = 0;
	Vector<unsigned char>			m_Data;
};

class BaseRequest : public BasePacket
{
public:
	virtual							~BaseRequest() {}

	virtual void					Serialize() = 0;
	virtual void					DeserializeResponse( unsigned char *pData, unsigned long nSize ) = 0;

	virtual bool					HasReply() { return 0; }
	virtual void					SetSequenceNum( unsigned long nSequenceNum ) { m_nSequenceNum = nSequenceNum; }
	virtual unsigned long			SequenceNum() { return m_nSequenceNum; }

	virtual unsigned long			GetResponse() { return m_ResponseResult; }
protected:
	HANDLE							m_hEvent = 0;
	unsigned long					m_nSequenceNum = 0;
	unsigned long					m_ResponseResult = 0;
};

class BaseResponse : public BasePacket
{
public:
	virtual							~BaseResponse() {}
	virtual void					Serialize() = 0;
};

class BaseRequestHandler: public BasePacket
{
public:
	virtual							~BaseRequestHandler() {}
	virtual unsigned long			GetType() { return m_PacketType; };
	virtual bool					DeserializePacket( BaseNetChannel *pChannel, buf_read &packet ) = 0;
};


/* ---- Response Packet ---- */
class Response : public BaseResponse
{
public:
	Response( void *pData, unsigned long nDataSize )
	{
		m_PacketType = RESPONSE;
		m_DataSize = nDataSize;
		m_ResponseData.PushBackMultiple( ( unsigned char * ) pData, nDataSize );
	}
	virtual					~Response() {}

	virtual void			Serialize()
	{
		m_nPacketSize = sizeof( m_PacketType ) + sizeof( m_nSequenceNum ) + sizeof( m_DataSize ) + m_DataSize;

		m_Data.Append( m_PacketType );
		m_Data.Append( m_nSequenceNum );
		m_Data.Append( m_DataSize );
		m_Data.PushBackMultiple( m_ResponseData.Base(), m_ResponseData.Length() );
	}
protected:
	Vector<unsigned char>	m_ResponseData;
};