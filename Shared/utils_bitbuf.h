#pragma once

class buf_write
{
public:
	void				Init( void *pData, int nBytes )				{ m_pData = (unsigned char *) pData; m_nDataBytes = nBytes; m_nCurByte = 0; }
	void				Reset()										{ m_nCurByte = 0; }

	template<class T>
	void				Write( const T &val )						{ Write( (const void *) &val, sizeof( val ) ); }
	void				Write( const void *pBuf, int nBytes );
	void				Write( const char *pStr )					{ Write( pStr, StrLenA( pStr ) + 1 ); }

	long				StrLenA( const char *pStr );
	void				Align( unsigned long nAlignment, unsigned char nFill );
	
	void				Seek( unsigned long nCurPos )				{ m_nCurByte = nCurPos; }
	void				SeekRelative( long nBytes )					{ m_nCurByte += nBytes; }

	unsigned char		*GetData() const							{ return m_pData; }
	unsigned long		GetSize() const								{ return m_nDataBytes; }
	unsigned long		GetNumBytesLeft() const						{ return m_nDataBytes - m_nCurByte; }
	unsigned long		GetNumBytesWritten() const					{ return m_nCurByte; }

protected:
	unsigned char		*m_pData;
	unsigned long		m_nDataBytes;
	unsigned long		m_nCurByte;
};

__forceinline void buf_write::Write( const void *pBuf, int nBytes )
{
	if ( !nBytes )
		return;

	int nStart = 0;

	do
	{
		if ( m_nCurByte >= m_nDataBytes )
			break;

		*(unsigned char *) ( m_pData + m_nCurByte++ ) = *(unsigned char *) ( (unsigned char *) pBuf + nStart );
		++nStart;
		--nBytes;
	}
	while ( nBytes );
}

__forceinline long buf_write::StrLenA( const char *pStr )
{
	long nLength = 0;

	while ( *pStr )
	{
		++nLength;
		++pStr;
	}

	return nLength;
}

__forceinline void buf_write::Align( unsigned long nAlignment, unsigned char nFill )
{
	while ( m_nCurByte % nAlignment )
		Write( (unsigned char) 0 );
}

class buf_read
{
public:
	void				Init( void *pData, int nBytes )				{ m_pData = (unsigned char *) pData; m_nDataBytes = nBytes; m_nCurByte = 0; }
	void				Reset()										{ m_nCurByte = 0; }

	template<class T>
	void				Read( T &val )								{ Read( (void *) &val, sizeof( val ) ); }
	void				Read( void *pOut, int nBytes );
	int					Read( char *pStr, int bufLen );
	
	void				Seek( unsigned long nCurPos )				{ m_nCurByte = nCurPos; }
	void				SeekRelative( long nBytes )					{ m_nCurByte += nBytes; }

	const unsigned char	*GetData() const							{ return m_pData; }
	unsigned long		GetSize() const								{ return m_nDataBytes; }
	unsigned long		GetNumBytesLeft() const						{ return m_nDataBytes - m_nCurByte; }
	unsigned long		GetNumBytesRead() const						{ return m_nCurByte; }

public:
	const unsigned char	*m_pData;
	unsigned long		m_nDataBytes;
	unsigned long		m_nCurByte;
};

__forceinline void buf_read::Read( void *pOut, int nBytes )
{
	int nStart = 0;

	do
	{
		if ( m_nCurByte >= m_nDataBytes )
			break;

		*(unsigned char *) ( (int) pOut + nStart ) = *(unsigned char *) ( m_pData + m_nCurByte++ );
		++nStart;
		--nBytes;
	}
	while ( nBytes );
}

__forceinline int buf_read::Read( char *pStr, int bufLen )
{
	int nStart = 0;
	char c = 0;

	do
	{
		Read( c );
		*(char *) ( pStr + nStart ) = c;
		++nStart;
	}
	while ( nStart < bufLen && c );

	return nStart - 1;
}
