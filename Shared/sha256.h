#pragma once

#define DIGEST_SIZE 32

void SHA256_DataToBinaryHash( const unsigned char *pInput, unsigned long nInputLength, unsigned char pOutput[ DIGEST_SIZE ] );
void SHA256_StringToBinaryHash( const char *pszInput, unsigned char pOutput[ DIGEST_SIZE ] );
void SHA256_StringToStringHash( const char *pszInput, char lpOutput[ 2 * DIGEST_SIZE + 1 ] );
void SHA256_BinaryToString( unsigned char pInput[ DIGEST_SIZE ], char pOutput[ 2 * DIGEST_SIZE + 1 ] );
void SHA256_StringToBinary( char lpInput[ 2 * DIGEST_SIZE + 1 ], unsigned char lpOutput[ DIGEST_SIZE ] );
