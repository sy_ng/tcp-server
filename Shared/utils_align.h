#pragma once

template<typename T>
inline T AlignDown( T x, unsigned long nAlignment )
{
	return x & ~( static_cast<T>(nAlignment) -1 );
}

template<typename T>
inline T AlignUp( T x, unsigned long nAlignment )
{
	return ( x & static_cast<T>( nAlignment - 1 ) ) ? AlignDown( x, nAlignment ) + static_cast<T>( nAlignment ) : x;
}

template<typename T>
inline bool IsPowerOf2( T x )
{
	return !( x & ( x - 1 ) );
}
