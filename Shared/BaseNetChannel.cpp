#include "BaseNetChannel.h"
#include "utils_bitbuf.h"
#include "BasePacket.h"


DWORD WINAPI Net_RunRemoteThread( void *pThisChannel )
{
	BaseNetChannel *pNetChannel = ( BaseNetChannel * ) pThisChannel;
	pNetChannel->Think();
	return 1;
}


bool BaseNetChannel::Shutdown()
{
	shutdown( m_Socket, SD_BOTH );
	closesocket( m_Socket );
	WSACleanup();
	return 1;
}

void BaseNetChannel::Think()
{
	bool bResult;

	while ( true )
	{
		m_TempSendBuf.Lock();
		if ( m_TempSendBuf.Length() )
		{
			//printf( "Buffer length is: %u\n", nLength );
			m_SendBuf.PushBackMultiple( m_TempSendBuf.Base(), m_TempSendBuf.Length() );
			m_TempSendBuf.Erase();
			m_TempSendBuf.Unlock();

			SendData( m_SendBuf );
			//if( !bResult )
			//	printf( "NetChannel: Error sending data\n" );
			m_SendBuf.Erase();
		}
		else
		{
			m_TempSendBuf.Unlock();
		}

		// Checking if any data ready to read
		FD_SET ReadSet;
		TIMEVAL Timeout = { 0, 0 };
		FD_ZERO( &ReadSet );
		FD_SET( m_Socket, &ReadSet );

		switch ( select( 0, &ReadSet, 0, NULL, &Timeout ) )
		{
		case SOCKET_ERROR:
		{
			printf( "NetChannel: failed to select with error:%d\n", WSAGetLastError() );
			Shutdown();
			return;
		}

		case 1:
		{
			m_nBytesRecieved = recv( m_Socket, m_pRecvBuf, m_nRecvBufSize, 0 );

			if ( m_nBytesRecieved == SOCKET_ERROR )
			{
				//printf( "NetChannel: Error while recieving data\n" );
				Shutdown();
				return;
			}
			bResult = DesirializeBuffer();
			if ( !bResult )
			{
				printf( "NetChannel: Cannot deserialize buffer\n" );
				return;
			}
			break;
		}

		case 0:
		{
			Sleep( 10 );
			break;
		}

		default:
			break;
		}
	}
}

bool BaseNetChannel::DesirializeBuffer()
{
	buf_read recvData;
	recvData.Init( m_pRecvBuf, m_nBytesRecieved );

	while ( recvData.GetNumBytesLeft() )
	{
		// add ret val check
		DeserializePacket( recvData );
	}
	return 1;
}

bool BaseNetChannel::DeserializePacket( buf_read &packet )
{
	unsigned long nRequestType;
	packet.Read( nRequestType );

	// Move this to request handler function
	switch ( nRequestType )
	{
	case CL_REQUEST_AUTH:
	case CL_REQUEST_REG:
	case REQUEST_SEND_MSG:
	{
		for ( unsigned int i = 0; i < m_RequestHandlers.Length(); i++ )
		{
			if ( m_RequestHandlers[ i ]->GetType() == nRequestType )
			{
				if ( !m_RequestHandlers[ i ]->DeserializePacket( (BaseNetChannel *) this, packet ) )
				{
					//printf( "NetChannel: cannot deserialize packet\n" );
					return 0;
				}
				else
					return 1;
			}
		}
	}

	case RESPONSE:
	{
		unsigned long nSequenceNum;
		unsigned long nDataSize;
	
		packet.Read( nSequenceNum );
		packet.Read( nDataSize );

		unsigned char *pData = new unsigned char[ nDataSize ];

		packet.Read( pData, nDataSize );

		m_SentRequests.Lock();
		for ( unsigned long i = 0; i < m_SentRequests.Length(); i++ )
		{
			if ( nSequenceNum == m_SentRequests[ i ]->SequenceNum() )
			{
				m_SentRequests[ i ]->DeserializeResponse( pData, nDataSize );
				m_SentRequests.Unlock();
				return 1;
			}
		}
		m_SentRequests.Unlock();
		return 0;
	}

	default:
		printf( "NetChannel: Failed to Deserlize, invalid request type\n" );
		return 0;
	}
}

void BaseNetChannel::RegisterHandler( BaseRequestHandler *pHandler )
{
	m_RequestHandlers.PushBack( pHandler );
}

bool BaseNetChannel::SendData( Vector<unsigned char> &SendBuffer )
{
	int nResult = send( m_Socket, ( const char * ) SendBuffer.Base(), SendBuffer.Length(), 0 );
	if ( nResult == SOCKET_ERROR )
	{
		printf( "NetChannel: send failed with error: %d\n", WSAGetLastError() );
		this->Shutdown();  // OnDisconnect callback must be here
		return 0;
	}
	//printf( "Bytes sent: %i\n", nResult );
	return 1;
}

void BaseNetChannel::SendRequest( BaseRequest &Request )
{
	Request.SetSequenceNum( m_nSequenceNum );
	++m_nSequenceNum;

	if ( Request.HasReply() )
	{
		m_SentRequests.PushBack( &Request );
	}

	Request.Serialize();
	m_TempSendBuf.PushBackMultiple( Request.Data().Base(), Request.Size() );
}

void BaseNetChannel::SendResponse( BaseResponse &Response )
{
	Response.SetSequenceNum( m_nSequenceNum );
	++m_nSequenceNum;

	Response.Serialize();
	m_TempSendBuf.PushBackMultiple( Response.Data().Base(), Response.Size() );
}
