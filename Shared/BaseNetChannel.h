#pragma once
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include "Vector.h"

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#define TEMP_BUFFER_SIZE 64 * 1024

class BaseRequest;
class BaseResponse;
class BaseRequestHandler;
class buf_read;


DWORD WINAPI Net_RunRemoteThread( void *pThisChannel );

class BaseNetChannel
{
public:
	BaseNetChannel()
	{	
		m_pRecvBuf = new char[ m_nRecvBufSize ];
	}
	virtual	~BaseNetChannel()
	{
		delete[] m_pRecvBuf;
	}
	
	virtual bool					Shutdown();

	virtual void					RegisterHandler( BaseRequestHandler *pHandler );

	virtual void					Think();

	virtual void					SendRequest( BaseRequest &Request );
	virtual void					SendResponse( BaseResponse &Response );
	virtual bool					SendData( Vector<unsigned char> &SendBuffer );

	virtual bool					DesirializeBuffer();
	virtual bool					DeserializePacket( buf_read &packet );

protected:
	addrinfo						*m_pAddress = NULL;
	SOCKET							m_Socket = INVALID_SOCKET;
	HANDLE							m_hThread = NULL;

	char							*m_pRecvBuf = NULL;
	unsigned long					m_nRecvBufSize = TEMP_BUFFER_SIZE;
	unsigned long					m_nBytesRecieved = 0;
	Vector<unsigned char>			m_SendBuf;
	Vector<unsigned char>			m_TempSendBuf;

	Vector<BaseRequest *>			m_SentRequests;
	unsigned long					m_nSequenceNum = 0;

	Vector<BaseRequestHandler *>	m_RequestHandlers;
};