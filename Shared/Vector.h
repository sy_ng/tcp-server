#pragma once

#include <Windows.h>
#include "CriticalSection.h"
#include "utils_align.h"
#include "stdio.h"

template <class T>
class Vector
{
private:
	unsigned long		m_nLength = 0;
	T *m_pData = nullptr;
	HANDLE				m_hHeap = GetProcessHeap();
	unsigned long		m_nCapacity = 0;
	CriticalSection		m_criticalSection;

public:
	Vector()
	{
		EnsureCapacity( m_nLength );
	}

	Vector( int length )
	{
		m_nLength = length;
		EnsureCapacity( length );
	}

	Vector( Vector<T> &vec )
	{
		m_nLength = vec.m_nLength;
		EnsureCapacity( m_nLength );
		memcpy( m_pData, vec.m_pData, vec.m_nCapacity );
	}

	~Vector()
	{
		HeapFree( m_hHeap, 0, m_pData );
	}

	T *Base()
	{
		return m_pData;
	}

	unsigned long Length()
	{
		return m_nLength;
	}

	void Erase()
	{
		SECTION_AUTOLOCK( m_criticalSection );
		//HeapFree( m_hHeap, 0, m_pData );
		//m_pData = nullptr;
		m_nCapacity = 0;
		m_nLength = 0;
	}

	void Erase( unsigned long nAt, unsigned long nSize )
	{
		SECTION_AUTOLOCK( m_criticalSection );
		if( nSize == 0 || ( nAt + nSize ) > m_nLength )
			return;

		unsigned long moveSize = m_nLength - ( nAt + nSize );
		if( moveSize == 0 )
		{
			m_nLength -= nSize;
			memset( ( void * ) ( &m_pData[ nAt ] ), 0, nSize * sizeof( T ) );
		}
		else
		{
			memmove( ( void * ) ( &m_pData[ nAt ] ), ( void * ) ( &m_pData[ nAt + nSize ] ), moveSize * sizeof( T ) );
			unsigned long emptySize = m_nLength - ( nAt + moveSize );
			memset( ( void * ) ( &m_pData[ nAt + moveSize ] ), 0, emptySize * sizeof( T ) );
			m_nLength -= nSize;
		}
	}

	bool EnsureCapacity( unsigned long length )
	{
		//SECTION_AUTOLOCK( m_criticalSection );
		LPVOID alloc_result;
		unsigned long req_capacity = length * sizeof( T ); // Required capacity

		if( req_capacity == 0 )
		{
			m_nCapacity = AlignUp( req_capacity, 4096 );
			alloc_result = ( T * ) HeapAlloc( m_hHeap, HEAP_ZERO_MEMORY, m_nCapacity );
			if( !alloc_result )
				return 0;
			m_pData = ( T * ) alloc_result;
		}
		else if( req_capacity > m_nCapacity )
		{
			m_nCapacity = AlignUp( req_capacity, 4096 );
			alloc_result = HeapReAlloc( m_hHeap, 0, m_pData, m_nCapacity );
			if( !alloc_result )
				return 0;
			m_pData = ( T * ) alloc_result;
		}
		return 1;
	}

	void PushBack( const T &element )
	{
		SECTION_AUTOLOCK( m_criticalSection );
		++m_nLength;
		bool bResult = EnsureCapacity( m_nLength );
		if( !bResult )
			return;
		memcpy( ( void * ) ( &m_pData[ m_nLength - 1 ] ), ( void * ) ( &element ), sizeof( T ) );
	}

	void PushBackMultiple( const T *pData, unsigned long length )
	{
		SECTION_AUTOLOCK( m_criticalSection );
		if( pData == 0 )
			return;

		EnsureCapacity( m_nLength + length );
		memcpy( ( void * ) ( &m_pData[ m_nLength ] ), ( void * ) pData, length * sizeof( T ) );
		m_nLength += length;
	}

	void Append( const char *pszString )
	{
		PushBackMultiple( ( const T * ) pszString, lstrlenA( pszString ) + 1 );
	}

	template<class C>
	void Append( const C &item )
	{
		PushBackMultiple( ( const T * ) &item, sizeof( item ) );
	}

	void PopBack()
	{
		SECTION_AUTOLOCK( m_criticalSection );
		--m_nLength;
		memset( ( void * ) ( &m_pData[ m_nLength ] ), 0, sizeof( T ) );
	}

	void Insert( unsigned long nAt, T *pInsertData, unsigned long nSize )
	{
		SECTION_AUTOLOCK( m_criticalSection );
		if( nAt > m_nLength || nSize == 0 || pInsertData == 0 )
			return;

		EnsureCapacity( m_nLength + nSize );
		unsigned long moveSize = m_nLength - nAt;

		if( moveSize != 0 )
			memmove( ( void * )( &m_pData[ nAt + nSize ] ), ( void * )( &m_pData[ nAt ] ), moveSize * sizeof( T ) );

		memcpy( ( void * ) ( &m_pData[ nAt ] ), ( void * ) pInsertData, nSize * sizeof( T ) );
		m_nLength += nSize;
	}

	void PrintAll()
	{
		SECTION_AUTOLOCK( m_criticalSection );
		for( unsigned long i = 0; i < m_nLength; i++ )
		{
			printf( "Element %u is: %u;\n", i, m_pData[ i ] );
		}
	}

	void Lock()
	{
		m_criticalSection.Lock();
	}

	void Unlock()
	{
		m_criticalSection.Unlock();
	}

	T &operator[]( unsigned int index )
	{
		if( index > m_nLength )
			return *( T * ) 0;

		return m_pData[ index ];
	}

	const T &operator[]( unsigned int index ) const
	{
		if( index > m_nLength )
			return *( T * ) 0;

		return m_pData[ index ];
	}
};