#include <Windows.h>

bool LoadFile( const char *pszFilePath, void **ppDataOut, unsigned long *pDataSizeOut )
{
	HANDLE hFile = CreateFileA( pszFilePath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0 );

	if ( hFile == INVALID_HANDLE_VALUE )
		return false;

	SetFilePointer( hFile, 0, 0, FILE_BEGIN );

	DWORD dwBytes;
	*pDataSizeOut = GetFileSize( hFile, NULL );
	*ppDataOut = new unsigned char[ *pDataSizeOut ];
	ReadFile( hFile, *ppDataOut, *pDataSizeOut, &dwBytes, NULL );
	CloseHandle( hFile );
	return true;
}

bool SaveFile( const char *pszFilePath, void *pData, unsigned long nDataSize )
{
	HANDLE hFile = CreateFileA( pszFilePath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0 );

	if ( hFile == INVALID_HANDLE_VALUE )
		return false;

	DWORD dwBytes;
	WriteFile( hFile, pData, nDataSize, &dwBytes, NULL );
	CloseHandle( hFile );
	return true;
}