#pragma once

bool LoadFile( const char *pszFilePath, void **ppDataOut, unsigned long *pDataSizeOut );
bool SaveFile( const char *pszFilePath, void *pData, unsigned long nDataSize );
